package com.example.fulltilt;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;

public class TiltedView extends View{
	
	private Paint paint;
	private float orientation = 0;
	
	public TiltedView(Context context){
		super(context);
		init();
	}

	private void init(){
		paint = new Paint();
	}
	
	public void updateData(float newOrientation){
		this.orientation=newOrientation;
		invalidate();
	}
	
}
