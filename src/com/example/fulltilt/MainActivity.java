package com.example.fulltilt;

import android.annotation.SuppressLint;
import android.app.Activity;
//import android.app.ActionBar;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
//import android.os.Build;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

	private static SensorManager sensorService;
	private Sensor rotSensor;
	private TextView xdisp;
	private TextView ydisp;
	private TextView zdisp;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);//set the view that the user see
		
		/*
		if (savedInstanceState == null) {
			getFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
		*/
		//get the sensors from the device
		sensorService = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		//get the rotation sensor
		rotSensor = sensorService.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
		
		//if rotsensor exists on the device register an event listener to listen for changes
		//otherwise log an error no gyro sensor was detected on device
		if(rotSensor!=null){
			sensorService.registerListener(mySensorEventListener, rotSensor, SensorManager.SENSOR_DELAY_NORMAL);
			Log.i("rot mainactivity","registerered for rot sensor");
		}else{
			Log.e("rot mainactivity","registerered for rot sensor");
			Toast.makeText(this,"rot sensor not found",Toast.LENGTH_LONG).show();
			finish();
		}
		
		//get text fields and set to 0 initially
		xdisp=(TextView)findViewById(R.id.x);
		ydisp=(TextView)findViewById(R.id.y);
		zdisp=(TextView)findViewById(R.id.z);
		xdisp.setText("0");
		ydisp.setText("0");
		zdisp.setText("0");
		//
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);
			return rootView;
		}
	}
	
	
	//event listener for when a sensor changes
	private SensorEventListener mySensorEventListener = new SensorEventListener(){
		
		
		
		@Override
		public void onAccuracyChanged(Sensor sensor,int accuracy){
			
		}
		
		//private static final float NS2S = 1.0f / 1000000000.0f;
	    //private final float[] deltaRotationVector = new float[4];
	    //private float timestamp;
		private float[] mRotMat=new float[9];
	    private float[] mOrientation=new float[3];
		
	    @SuppressLint("FloatMath")
		@Override
		public void onSensorChanged(SensorEvent event){
	    	if(event.sensor.getType()==Sensor.TYPE_ROTATION_VECTOR){
	    		
	    		//get rotation matrix
	    		SensorManager.getRotationMatrixFromVector(mRotMat,event.values);
	    		//get orienation based on rotation matrix
	    		SensorManager.getOrientation(mRotMat,mOrientation);
	    		
	    		//display rotation angle in x y z 
	    		xdisp.setText(String.valueOf(mOrientation[0]));
	    		ydisp.setText(String.valueOf(mOrientation[1]));
	    		zdisp.setText(String.valueOf(mOrientation[2]));
	    	}
			
		
		}
		
		
	};

}
